package com.zsCat.common.beetl.function;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.zsCat.web.goods.model.Goods;
import com.zsCat.web.goods.model.GoodsClass;
import com.zsCat.web.goods.model.ShopType;
import com.zsCat.web.goods.service.GoodsClassService;
import com.zsCat.web.goods.service.GoodsService;
import com.zsCat.web.goods.service.ShopTypeService;

@Component
public class GoodsClassFunctions {
	
	@Resource
	private GoodsClassService goodsClassService;
	@Resource
	private ShopTypeService ShopTypeService;
	@Resource
	private GoodsService GoodsService;
	public List<GoodsClass> getGoodsClassListByPid(Long pid){
		GoodsClass gc=new GoodsClass();
		gc.setParentId(pid);
		return goodsClassService.select(gc);
	}
	public List<ShopType> getShopTypeListByPid(Long pid){
		ShopType gc=new ShopType();
		gc.setParentId(pid);
		return ShopTypeService.select(gc);
	}
	public List<Goods> getGoodsListByTypeid(Long tid){
		Goods goods=new Goods();
		goods.setTypeId(tid);
		return GoodsService.selectPage(1, 6, goods).getList();
	}
	
}
