//Powered By ZSCAT, Since 2014 - 2020

package com.zsCat.web.goods.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import com.zsCat.common.base.ServiceMybatis;
import com.zsCat.web.goods.service.ShopTypeService;
import com.zsCat.web.goods.mapper.ShopTypeMapper;

import com.zsCat.web.goods.model.ShopType;
import com.zsCat.web.sys.model.SysOffice;
import com.zsCat.web.sys.model.SysRole;

/**
 * 
 * @author
 */

@Service("ShopTypeService")
public class ShopTypeServiceImpl  extends ServiceMybatis<ShopType> implements ShopTypeService {

	@Resource
	private ShopTypeMapper ShopTypeMapper;

	
	/**
	 * 保存或更新
	 * 
	 * @param ShopType
	 * @return
	 */
	public int saveShopType(ShopType ShopType) {
		return this.save(ShopType);
	}

	/**
	 * 删除
	* @param ShopType
	* @return
	 */
	public int deleteShopType(ShopType ShopType) {
		return this.delete(ShopType);
	}


}
