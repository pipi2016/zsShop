//Powered By ZSCAT, Since 2014 - 2020

package com.zsCat.web.goods.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import com.zsCat.common.base.ServiceMybatis;
import com.zsCat.web.goods.service.GoodsService;
import com.zsCat.web.goods.mapper.GoodsMapper;

import com.zsCat.web.goods.model.Goods;
import com.zsCat.web.sys.model.SysOffice;
import com.zsCat.web.sys.model.SysRole;

/**
 * 
 * @author
 */

@Service("GoodsService")
public class GoodsServiceImpl  extends ServiceMybatis<Goods> implements GoodsService {

	@Resource
	private GoodsMapper GoodsMapper;

	
	/**
	 * 保存或更新
	 * 
	 * @param Goods
	 * @return
	 */
	public int saveGoods(Goods Goods) {
		return this.save(Goods);
	}

	/**
	 * 删除
	* @param Goods
	* @return
	 */
	public int deleteGoods(Goods Goods) {
		return this.delete(Goods);
	}


}
