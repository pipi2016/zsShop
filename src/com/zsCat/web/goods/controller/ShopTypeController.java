package com.zsCat.web.goods.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.zsCat.web.goods.model.ShopType;
import com.zsCat.web.goods.service.ShopTypeService;
	/**
	 * 
	 * @author zs 2016-5-24 21:52:07
	 * @Email: 951449465@qq.com
	 * @version 4.0v
	 *	我的cms
	 */
@Controller
@RequestMapping("cmsCategory")
public class ShopTypeController {

	@Resource
	private ShopTypeService ShopTypeService;
	
	@RequestMapping
	public String toArea(Model model) {
		model.addAttribute("treeList", JSON.toJSONString(ShopTypeService.select(null)));
		return "cms/cmsCategory/cmsCategory";
	}

	/**
	 * 区域树
	 * @return
	 */
	@RequestMapping(value = "tree", method = RequestMethod.POST)
	public @ResponseBody List<ShopType> getAreaTreeList() {
		List<ShopType> list = ShopTypeService.select(null);
		return list;
	}
	
	/**
	 * 添加或更新区域
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public @ResponseBody Integer save(@ModelAttribute ShopType ShopType,HttpServletRequest request) {
		return ShopTypeService.saveShopType(ShopType);
	}
	
	/**
	 * 删除字典
	* @param id
	* @return
	 */
	@RequestMapping(value="delete",method=RequestMethod.POST)
	public @ResponseBody Integer del(@ModelAttribute ShopType ShopType){
		return ShopTypeService.deleteShopType(ShopType);
	}
	
	/**
	 * 分页显示字典table
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public String list(int pageNum,int pageSize,@ModelAttribute ShopType ShopType, Model model) {
		PageInfo<ShopType> page = ShopTypeService.selectPage(pageNum, pageSize, ShopType);
		model.addAttribute("page", page);
		return "cms/cmsCategory/cmsCategory-list";
	}
	

	@RequestMapping(value = "{mode}/showlayer", method = RequestMethod.POST)
	public String showLayer(Long id, Long parentId,
			@PathVariable("mode") String mode, Model model) {
		ShopType cmsCategory = null, pShopType = null;
		if (StringUtils.equalsIgnoreCase(mode, "add")) {
			pShopType = ShopTypeService.selectByPrimaryKey(parentId);
		} else if (StringUtils.equalsIgnoreCase(mode, "edit")) {
			cmsCategory = ShopTypeService.selectByPrimaryKey(id);
			pShopType = ShopTypeService.selectByPrimaryKey(parentId);
		} else if (StringUtils.equalsIgnoreCase(mode, "detail")) {
			cmsCategory = ShopTypeService.selectByPrimaryKey(id);
			pShopType = ShopTypeService.selectByPrimaryKey(cmsCategory.getParentId());
		}
		model.addAttribute("pShopType", pShopType).addAttribute("cmsCategory", cmsCategory);
		return mode.equals("detail") ? "cms/cmsCategory/cmsCategory-detail"
				: "cms/cmsCategory/cmsCategory-save";
	}
	
}
