//Powered By ZSCAT, Since 2014 - 2020

package com.zsCat.web.goods.model;

import java.util.Date;
import java.math.BigDecimal;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.zsCat.common.base.BaseEntity;


/**
 * 
 * @author zsCat 2016-10-31 16:32:55
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	商品管理
 */
@SuppressWarnings({ "unused"})
@Table(name="shop_type")
public class ShopType extends BaseEntity {

	private static final long serialVersionUID = 1L;
		 private String title;
public String getTitle() {return this.getString("title");}
public void setTitle(String title) {this.set("title",title);}
  		 private String typeName;
public String getTypeName() {return this.getString("typeName");}
public void setTypeName(String typeName) {this.set("typeName",typeName);}
private Integer typeSort;
public Integer getTypeSort() {return this.getInteger("typeSort");}
public void setTypeSort(Integer typeSort) {this.set("typeSort",typeSort);}
private Long parentId;
public Long getParentId() {return this.getLong("parentId");}
public void setParentId(Long parentId) {this.set("parentId",parentId);}
private String stIdpath;
public String getStIdpath() {return this.getString("stIdpath");}
public void setStIdpath(String stIdpath) {this.set("stIdpath",stIdpath);}
private Integer expenScale;
public Integer getExpenScale() {return this.getInteger("expenScale");}
public void setExpenScale(Integer expenScale) {this.set("expenScale",expenScale);}
private String parentIds;
public String getParentIds() {return this.getString("parentIds");}
public void setParentIds(String parentIds) {this.set("parentIds",parentIds);}

@Transient
private String oldParentIds; //旧的pids,非表中字段，用作更新用
  public String getOldParentIds() {
		return this.getString("oldParentIds");
  }
 
  public void setOldParentIds(String oldParentIds) {
		this.set("oldParentIds", oldParentIds);
  }

}
