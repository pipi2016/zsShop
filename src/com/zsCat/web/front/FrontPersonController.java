package com.zsCat.web.front;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.zsCat.web.goods.model.Goods;
import com.zsCat.web.goods.model.GoodsClass;
import com.zsCat.web.goods.model.ShopType;
import com.zsCat.web.goods.service.GoodsClassService;
import com.zsCat.web.goods.service.GoodsService;
import com.zsCat.web.goods.service.ShopTypeService;
	/**
	 * 
	 * @author zsCat 2016-10-31 14:01:30
	 * @Email: 951449465@qq.com
	 * @version 4.0v
	 *	商品管理
	 */
@Controller
@RequestMapping("/person")
public class FrontPersonController {

	@Resource
	private GoodsClassService GoodsClassService;
	@Resource
	private GoodsService GoodsService;
	@Resource
	private ShopTypeService ShopTypeService;
	
	 @RequestMapping("")
	  public ModelAndView index() {
	        try {
	            ModelAndView model = new ModelAndView("/mall/person/index");
	            Goods goods=new Goods();
	            PageInfo<Goods> page = GoodsService.selectPage(1, 4, goods);
	            model.addObject("page", page);
	            GoodsClass gc=new GoodsClass();
	            gc.setParentId(1L);
	            List<GoodsClass> gcList=GoodsClassService.selectPage(1, 15, gc).getList();
	            ShopType sp=new ShopType();
	            sp.setParentId(1L);
	            List<ShopType> spList=ShopTypeService.selectPage(1, 9, sp).getList();
	            model.addObject("spList", spList);
	            model.addObject("gcList", gcList);
	            return model;
	        } catch (Exception e) {
	            e.printStackTrace();
	            throw new RuntimeException("导航失败!");
	        }
	    }
	 /**
	  * 个人信息
	  * @return
	  */
	 @RequestMapping("/information")
	  public ModelAndView information() {
		 ModelAndView model = new ModelAndView("/mall/person/information");
		 
		 
		 return model;
	 }
	  /**
	  * 安全管理
	  * @return
	  */
	 @RequestMapping("/safety")
	  public ModelAndView safety() {
		 ModelAndView model = new ModelAndView("/mall/person/safety");
		 
		 
		 return model;
	 }
	 /**
	  * 地址管理
	  * @return
	  */
	 @RequestMapping("/address")
	  public ModelAndView address() {
		 ModelAndView model = new ModelAndView("/mall/person/address");
		 
		 
		 return model;
	 }
	 /**
	  * 快捷支付
	  * @return
	  */
	 @RequestMapping("/cardlist")
	  public ModelAndView cardlist() {
		 ModelAndView model = new ModelAndView("/mall/person/cardlist");
		 
		 
		 return model;
	 }
	 /**
	  * 订单管理
	  * @return
	  */
	 @RequestMapping("/order")
	  public ModelAndView order() {
		 ModelAndView model = new ModelAndView("/mall/person/order");
		 
		 
		 return model;
	 }
	 /**
	  * 退款管理
	  * @return
	  */
	 @RequestMapping("/change")
	  public ModelAndView change() {
		 ModelAndView model = new ModelAndView("/mall/person/change");
		 
		 
		 return model;
	 }
	 /**
	  * 评价管理
	  * @return
	  */
	 @RequestMapping("/comment")
	  public ModelAndView comment() {
		 ModelAndView model = new ModelAndView("/mall/person/comment");
		 
		 
		 return model;
	 }
	 /**
	  * 我的积分
	  * @return
	  */
	 @RequestMapping("/points")
	  public ModelAndView points() {
		 ModelAndView model = new ModelAndView("/mall/person/points");
		 
		 
		 return model;
	 }
	 
	 /**
	  * 优惠劵
	  * @return
	  */
	 @RequestMapping("/coupon")
	  public ModelAndView coupon() {
		 ModelAndView model = new ModelAndView("/mall/person/coupon");
		 
		 
		 return model;
	 }
	 /**
	  * 红包
	  * @return
	  */
	 @RequestMapping("/bonus")
	  public ModelAndView bonus() {
		 ModelAndView model = new ModelAndView("/mall/person/bonus");
		 
		 
		 return model;
	 }
	 /**
	  * 账户余额
	  * @return
	  */
	 @RequestMapping("/walletlist")
	  public ModelAndView walletlist() {
		 ModelAndView model = new ModelAndView("/mall/person/walletlist");
		 
		 
		 return model;
	 }
	 /**
	  * 账单明细
	  * @return
	  */
	 @RequestMapping("/bill")
	  public ModelAndView bill() {
		 ModelAndView model = new ModelAndView("/mall/person/bill");
		 
		 
		 return model;
	 }
	 /**
	  * 收藏
	  * @return
	  */
	 @RequestMapping("/collection")
	  public ModelAndView collection() {
		 ModelAndView model = new ModelAndView("/mall/person/collection");
		 
		 
		 return model;
	 }
	 /**
	  * 足迹
	  * @return
	  */
	 @RequestMapping("/foot")
	  public ModelAndView foot() {
		 ModelAndView model = new ModelAndView("/mall/person/foot");
		 
		 
		 return model;
	 }
	 /**
	  * 商品咨询
	  * @return
	  */
	 @RequestMapping("/consultation")
	  public ModelAndView consultation() {
		 ModelAndView model = new ModelAndView("/mall/person/consultation");
		 
		 
		 return model;
	 }
	 /**
	  * 意见反馈
	  * @return
	  */
	 @RequestMapping("/suggest")
	  public ModelAndView suggest() {
		 ModelAndView model = new ModelAndView("/mall/person/suggest");
		 
		 
		 return model;
	 }
	 /**
	  * 我的消息
	  * @return
	  */
	 @RequestMapping("/news")
	  public ModelAndView news() {
		 ModelAndView model = new ModelAndView("/mall/person/news");
		 
		 
		 return model;
	 }
	
}
