package com.zsCat.web.front;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.zsCat.web.goods.model.Goods;
import com.zsCat.web.goods.service.GoodsService;
	/**
	 * 
	 * @author zsCat 2016-10-31 13:59:18
	 * @Email: 951449465@qq.com
	 * @version 4.0v
	 *	商品管理
	 */
@Controller
@RequestMapping("front/goods")
public class GoodsFrontController {

	@Resource
	private GoodsService GoodsService;
	
	
	@RequestMapping("/goodsDetail/{id}")
	public ModelAndView goodsDetail(@PathVariable("id") Long id)throws Exception{
		ModelAndView mav=new ModelAndView();
		Goods b=GoodsService.selectByPrimaryKey(id);
		mav.addObject("goods", b);
		mav.setViewName("mall/goodsDetail");
		return mav;
	}
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/goodsListBygcId/{gcId}")
	public ModelAndView goodsListBygcId(@PathVariable("gcId") Long gcId)throws Exception{
		ModelAndView mav=new ModelAndView();
		Goods g=new Goods();
		g.setGcId(gcId);
		PageInfo<Goods> page=GoodsService.selectPage(1, 40, g);
		mav.addObject("page", page);
		mav.setViewName("mall/search");
		return mav;
	}
	
	@RequestMapping("/goodsListBygcTypeId/{typeId}")
	public ModelAndView goodsListBygcTypeId(@PathVariable("typeId") Long typeId)throws Exception{
		ModelAndView mav=new ModelAndView();
		Goods g=new Goods();
		g.setTypeId(typeId);
		PageInfo<Goods> page=GoodsService.selectPage(1, 40, g);
		mav.addObject("page", page);
		mav.setViewName("mall/search");
		return mav;
	}
	
}
